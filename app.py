from tkinter import *
from LeChiffreDeCesar import *
import pyperclip

#Définition des fonctions déclenchées au clic des boutons

def button_cryptageSimple_callback():
    cleCryptage = int(saisieCleCryptage.get())
    messageToCrypter = saisieToCrypter.get()
    messageCrypte = cryptageSimple(messageToCrypter, cleCryptage)
    zoneTexteResultat.delete(0, END)
    zoneTexteResultat.insert(0, messageCrypte)

def button_decryptageSimple_callback():
    cleCryptage = int(saisieCleCryptage.get())
    messageToCrypter = saisieToCrypter.get()
    messageCrypte = decryptageSimple(messageToCrypter, cleCryptage)
    zoneTexteResultat.delete(0, END)
    zoneTexteResultat.insert(0, messageCrypte)

def button_cryptageCirculaire_callback():
    cleCryptage = int(saisieCleCryptage.get())
    messageToCrypter = saisieToCrypter.get()
    messageCrypte = cryptageCirculaire(messageToCrypter, cleCryptage)
    zoneTexteResultat.delete(0, END)
    zoneTexteResultat.insert(0, messageCrypte)

def button_decryptageCirculaire_callback():
    cleCryptage = int(saisieCleCryptage.get())
    messageToCrypter = saisieToCrypter.get()
    messageCrypte = decryptageCirculaire(messageToCrypter, cleCryptage)
    zoneTexteResultat.delete(0, END)
    zoneTexteResultat.insert(0, messageCrypte)

def button_copiePressePapier_callback():
    pyperclip.copy(zoneTexteResultat.get())

#Paramétrage de la fenêtre 
fenetrePrincipale = Tk()
fenetrePrincipale.title('Le Chiffre De Cesar')

fenetrePrincipale.rowconfigure(5, weight=1)
fenetrePrincipale.columnconfigure(7, weight=1)  

#création du champ de saisie du code à crypter
saisieToCrypter = Entry(fenetrePrincipale)
saisieToCrypter.grid(row=2, column=1, columnspan=3, padx=10, pady=10, ipady=3) # cf https://effbot.org/tkinterbook/grid.htm

#Création du champs servant à initialiser la clé de cryptage
saisieCleCryptage = Entry(fenetrePrincipale)
saisieCleCryptage.grid(row=2, column=4)

#Création du label pour la clé de cryptage
labelCleCryptage = Label(fenetrePrincipale, text = "Saisissez la clé de cryptage")
labelCleCryptage.grid(row=1, column=4)
labelCleCryptage['fg'] = "blue"

#création du champ de résultat
zoneTexteResultat = Entry(fenetrePrincipale)
zoneTexteResultat.grid(row=2, column=5, columnspan=7, padx=10, pady=10, ipady=3)

#création du label titre de la zone de saisie.
titreLabelSaisie = Label(fenetrePrincipale, text='Saisissez le texte à crypter :')
titreLabelSaisie.grid(column=1, row=1, columnspan=3)
titreLabelSaisie['fg']="blue"  #couleur du label 

#création du label2
titreLabelAffichage = Label(fenetrePrincipale, text='Votre texte crypté :')
titreLabelAffichage.grid(row=1, column=5, columnspan=7)
titreLabelAffichage['fg']="blue"  #couleur du label

#Boutons à créer : crypter, décrypter, simple, circulaire
boutonCrypter = Button(fenetrePrincipale, text='Cryptage Simple', command = button_cryptageSimple_callback)
boutonCrypter.grid(row=5,sticky='ew', column=1, padx=3, pady=3)

boutonDecrypter = Button(fenetrePrincipale, text='Décryptage Simple', command = button_decryptageSimple_callback)
boutonDecrypter.grid(row=5,sticky='ew', column=2, padx=3, pady=3)

boutonCryptageCirculaire = Button(fenetrePrincipale, text='Cryptage Circulaire', command = button_cryptageCirculaire_callback)
boutonCryptageCirculaire.grid(row=5,sticky='ew', column=4, padx=3, pady=3)

boutonDecryptageCirculaire = Button(fenetrePrincipale, text='Décryptage Circulaire', command = button_decryptageCirculaire_callback)
boutonDecryptageCirculaire.grid(row=5,  column=5, padx=3, pady=3)

boutonCopier = Button(fenetrePrincipale, text='Copier', command = button_copiePressePapier_callback)
boutonCopier.grid(row=5,  column=7, padx=3, pady=3)
boutonCopier['fg']='red'

fenetrePrincipale.mainloop()  #loop = La fenêtre reste ouverte indéfiniment