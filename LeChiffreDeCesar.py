#Listes des lettres de l'alphabet
alphabetMaj = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' ]
alphabetMin = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' ]

#---

#Fonction prenant en paramètre un caractère quelconque
#Retourne sa valeur décimale dans la table ASCII
def getAsciiCode(lettre):
    return ord(lettre)

#---

#Fonction prenant en paramètre un caractère et la clé de cryptage
#retourne la lettre cryptée
def getLettre(lettre, cleCryptage):
    return chr(getAsciiCode(lettre)+cleCryptage)

#--- 

#Fonction de chiffrement simple (non-circulaire)
#Prend en paramètre la chaîne à crypter et la clé de cryptage
#Retourne une chaîne cryptée
def cryptageSimple(chaineCaractere, cle):
    
    messageCrypte = ""

    for lettre in chaineCaractere:
        messageCrypte = messageCrypte + chr(getAsciiCode(lettre) + cle)
    
    return messageCrypte

#---

#Fonction prenant en paramètre la chaine de caractères à décrypter et la clé de décryptage
#Retourne le message décrypté selon un décryptage classique (non-circulaire)
def decryptageSimple(chaineCaractere, cle):
    
    messageDecryptee = ""

    for lettre in chaineCaractere:
        messageDecryptee = messageDecryptee + chr(getAsciiCode(lettre) - cle)

    return messageDecryptee

#---

#Fonction qui prend en paramètre un caractère, une clé de cryptage et un caractère repère
#La fonction retourne de combien est décalé le caractère crypté par rapport au caractère repère
def deCombienJeDepasse(lettre, cleCryptage, lettreTerminale):
    return (getAsciiCode(chr(getAsciiCode(lettre)+cleCryptage))-getAsciiCode(lettreTerminale))%26

#--- 

#La fonction permet de crypter un caractère si celui-ci est une minuscule
#Prend en paramètre le caractère et la clé de cryptage
#Retourne le caractère crypté
def cryptageCirculaireMin(lettre, cleCryptage):
    messageCrypte = ""
    if deCombienJeDepasse(lettre, cleCryptage, 'z')>0:
        messageCrypte = messageCrypte + cryptageSimple('a', deCombienJeDepasse(lettre, cleCryptage, 'z')-1)
    else:
        messageCrypte = messageCrypte + cryptageSimple(lettre, cleCryptage)
    return messageCrypte

#---

#La fonction permet de crypter un caractère si celui-ci est une majuscule -> Cryptage circulaire
#La fonction étudié d'abord où se trouve le caractère crypté. Si c'est après 'Z', on repart de A.
#Du code ASCII de 'A', on ajoute la différence de Z à la lettre hors de l'alphabet qui allait être cryptée.
#Prend en paramètre le caractère et la clé de cryptage
#Retourne le caractère crypté
#Par exemple : cryptageCirculaireMaj('Z', 10) -> J
def cryptageCirculaireMaj(lettre, cleCryptage):
    messageCrypte = ""
    if deCombienJeDepasse(lettre, cleCryptage, 'Z')>0:
        messageCrypte = messageCrypte + cryptageSimple('A', deCombienJeDepasse(lettre, cleCryptage, 'Z')-1)
    else:
        messageCrypte = messageCrypte + cryptageSimple(lettre, cleCryptage)
    return messageCrypte

#--- 

#La fonction permet de crypter une chaîne de caractère -> Cryptage circulaire
#Prend en paramètre une chaîne de caractères et une clé de cryptage
#Renvoie un message crypté
def cryptageCirculaire(chaineCaractere, cleCryptage):
    messageCrypte = ""
    for lettre in chaineCaractere:
        if lettre in alphabetMin: 
            messageCrypte = messageCrypte + cryptageCirculaireMin(lettre, cleCryptage)
        elif lettre in alphabetMaj:
            messageCrypte = messageCrypte + cryptageCirculaireMaj(lettre, cleCryptage)
        else:
            messageCrypte = messageCrypte + lettre

    return messageCrypte

#---

#La fonction permet de calculer le décalage entre un caractère crypté et un caractère
#Prend en paramètre la lettre à crypter, la clé de cryptage et la lettre servant de repère
#Retourne la valeur du décalage du caractère crypté par rapport à la valeur repère
def deCombienJeDepasse_Decryptage(lettre, cleCryptage, lettreTerminale):
    valeurAsciiLettre = getAsciiCode(lettre)
    valeurAsciiLettreCryptee = getAsciiCode(chr(valeurAsciiLettre-cleCryptage))
    valeurDecalage = valeurAsciiLettreCryptee - getAsciiCode(lettreTerminale)

    return valeurDecalage

#--- 

#La fonction permet de récupérer une valeur décryptée d'un caractère en MINUSCULE
#La fonction prend en paramètre la lettre à décrypter et la clé de décryptage
#Retourne le caractère décrypté
def decryptageCirculaireMin(lettre, cleCryptage):
    messageCrypte = ""
    if deCombienJeDepasse_Decryptage(lettre, cleCryptage, 'a')<0:
        messageCrypte = messageCrypte + decryptageSimple('z', abs(deCombienJeDepasse_Decryptage(lettre, cleCryptage, 'a'))%26-1)
    else:
        messageCrypte = messageCrypte + decryptageSimple(lettre, cleCryptage)
    return messageCrypte

#---

#La fonction permet de récupérer une valeur décryptée d'un caractère en MAJUSCULE
#La fonction prend en paramètre la lettre à décrypter et la clé de décryptage
#Retourne le caractère décrypté
def decryptageCirculaireMaj(lettre, cleCryptage):
    messageCrypte = ""
    if deCombienJeDepasse_Decryptage(lettre, cleCryptage, 'A')<0:
        messageCrypte = messageCrypte + decryptageSimple('Z', abs(deCombienJeDepasse_Decryptage(lettre, cleCryptage, 'A'))%26-1)
    else:
        messageCrypte = messageCrypte + decryptageSimple(lettre, cleCryptage)
    return messageCrypte

#--- 

#La fonction permet de décrypter une chaîne de caractères
#Elle prend en paramètre la chaîne de caractères à décrypter et la clé de cryptage
#Retourne une chaîne décryptée
def decryptageCirculaire(chaineCaractere, cleCryptage):
    messageCrypte = ""
    for lettre in chaineCaractere:
        if lettre in alphabetMin: 
            messageCrypte = messageCrypte + decryptageCirculaireMin(lettre, cleCryptage)
        elif lettre in alphabetMaj:
            messageCrypte = messageCrypte + decryptageCirculaireMaj(lettre, cleCryptage)
        else:
            messageCrypte = messageCrypte + lettre

    return messageCrypte