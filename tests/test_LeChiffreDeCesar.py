import unittest

from LeChiffreDeCesar import *

class TestLeChiffreDeCesar(unittest.TestCase):

    def test_getAsciiCode(self):
        self.assertEqual(getAsciiCode('A'), 65)

    def test_getLettre(self):
        self.assertEqual(getLettre('A', 2), 'C')

    def test_cryptageSimple(self):
        self.assertEqual(cryptageSimple('Marine', 7), 'Thypul')

    def test_decryptageSimple(self):
        self.assertEqual(decryptageSimple('qevmri', 7), 'j^ofkb')

    def test_deCombienJedepasse(self):
        self.assertEqual(deCombienJeDepasse('x', 8, 'z'), 6)

    def test_cryptageCirculaireMin(self):
        self.assertEqual(cryptageCirculaireMin('i', 8), 'q')
        self.assertEqual(cryptageCirculaireMin('x', 10), 'h')

    def test_cryptageCirculaireMaj(self):
        self.assertEqual(cryptageCirculaireMaj('I', 8), 'Q')
        self.assertEqual(cryptageCirculaireMaj('X', 10), 'H')

    def test_cryptageCirculaire(self):
        self.assertEqual(cryptageCirculaire('Karim', 12), 'Wmduy')

    def test_DeCombienJeDepasseCaract_Decryptage(self):
        self.assertEqual(deCombienJeDepasse_Decryptage('h', 12, 'z'), -30)

    def test_DecryptageCirculaireMin(self):
        self.assertEqual(decryptageCirculaireMin('h', 12), 'v')

    def test_DecryptageCirculaireMaj(self):
        self.assertEqual(decryptageCirculaireMaj('H', 20), 'N')

    def test_decryptageCirculaire(self):
        self.assertEqual(decryptageCirculaire('Wmduy', 12), 'Karim')